<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallePropiedadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_propiedades', function (Blueprint $table) {
            $table->id("id_detalle_propiedad");
            $table->text("descripcion");
            $table->unsignedBigInteger("id_propiedad");
            $table->foreign('id_propiedad', 'fk_detalle_propiedad')->references('id_propiedad')->on('propiedades')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles_propiedades');
    }
}
