<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropiedadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propiedades', function (Blueprint $table) {
            $table->id('id_propiedad');
            $table->text('titulo');
            $table->text('descripcion');
            $table->double('precio', 12, 2);
            $table->text('localizacion');
            $table->string('area', 50);
            $table->smallInteger('cuartos');
            $table->smallInteger('banios');
            $table->smallInteger('garages');
            $table->text('slug');
            $table->unsignedBigInteger('id_categoria');
            $table->foreign('id_categoria', 'fk_categoria_propiedad')->references('id_categoria')->on('categorias')->onDelete('restrict')->onUpdate('restrict');            
            $table->unsignedBigInteger('id_agente');
            $table->foreign('id_agente', 'fk_agente_propiedad')->references('id_agente')->on('agentes')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propiedades');
    }
}
