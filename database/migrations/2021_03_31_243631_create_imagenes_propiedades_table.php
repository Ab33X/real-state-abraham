<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagenesPropiedadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagenes_propiedades', function (Blueprint $table) {
            $table->id("id_imagen_propiedad");
            $table->text("titulo_imagen");
            $table->text("nombre_archivo");
            $table->string("extension", 10);
            $table->unsignedBigInteger('id_propiedad');
            $table->foreign('id_propiedad', 'fk_imagen_propiedad')->references('id_propiedad')->on('propiedades')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagenes_propiedades');
    }
}
