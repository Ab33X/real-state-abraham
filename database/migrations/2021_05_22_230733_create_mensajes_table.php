<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajes', function (Blueprint $table) {
            $table->id("id_mensaje");
            $table->text("nombre");
            $table->text("correo");
            $table->text("comentario");
            $table->unsignedBigInteger("id_propiedad");
            $table->foreign("id_propiedad", "fk_propiedad_mensaje")->references("id_propiedad")->on("propiedades")->onDelete("restrict")->onUpdate("restrict");            
            $table->unsignedBigInteger("id_agente");
            $table->foreign("id_agente", "fk_agente_mensaje")->references("id_agente")->on("agentes")->onDelete("restrict")->onUpdate("restrict");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensajes');
    }
}
