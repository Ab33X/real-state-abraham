@extends('templates/admin_template')

@php
    $title = 'Registrar';
    $id_noticia = '';
    $titulo = '';
    $subtitulo = '';
    $fecha_publicacion = '';
    $imagen = '';
    if(isset($noticia)){
        $title = 'Actualizar';
        
        $id_noticia = $noticia->id_noticia;
        $titulo = $noticia->titulo;
        $subtitulo = $noticia->subtitulo;
        $fecha_publicacion = $noticia->fecha_publicacion;
        $imagen = $noticia->imagen;

    }
@endphp

@section('title', 'Admin - '.$title.' noticias')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>{{$title}} noticias
                    <div class="page-title-subheading">Rellena el formulario
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a role="button" href="{{ url('admin/noticias') }}" data-toggle="tooltip" title="Regresar"
                    data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fa fa-arrow-left"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="main-card mb-3 card w-50">
            <div class="card-body">
                <form class="" method='POST' action='{{ url('/admin/noticias') }}' enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_noticia" value="{{$id_noticia}}">
                    <input type="hidden" name="imagen" value="{{$imagen}}">
                    <div class="position-relative form-group">
                        <label for="titulo" class="">Titulo de la noticia</label>
                        <input name="titulo" id="titulo" type="text" value="{{$titulo}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="subtitulo" class="">Subtitulo de la noticia</label>
                        <input name="subtitulo" id="subtitulo" type="text" value="{{$subtitulo}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="fecha_publicacion" class="">Fecha de publicacion de la noticia</label>
                        <input name="fecha_publicacion" id="fecha_publicacion" type="date" value="{{$fecha_publicacion}}" class="form-control">
                    </div>                     
                    <div class="position-relative form-group">
                        <label for="fileImage" class="">Imagen de la noticia</label>
                        <input name="fileImage" id="fileImage" type="file" class="form-control">
                    </div>
                    <button class="mt-1 btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection