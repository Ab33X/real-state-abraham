@extends('templates/admin_template')

@section('title', 'Admin - Noticias')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Noticias
                    <div class="page-title-subheading">Administra aqui las noticias que mostrará el sistema.
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    
                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        class="btn-shadow dropdown-toggle btn btn-dark">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-business-time fa-w-20"></i>
                        </span>
                        Acciones
                    </button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a href="{{ url('admin/noticias/new')}}" class="nav-link">
                                    <i class="nav-link-icon lnr-inbox"></i>
                                    <span>
                                        Nuevo
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="main-card mb-3 card w-100">
            <div class="card-body">
                <table class="mb-0 table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Titulo</th>
                            <th>Subtitulo</th>
                            <th>Fecha publicacion</th>
                            <th>Imagen</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($noticias->count() > 0)
                        @foreach ($noticias as $noticia)
                        <tr>
                            <th scope="row">{{$noticia->id_noticia}}</th>
                            <td>{{$noticia->titulo}}</td>
                            <td>{{$noticia->subtitulo}}</td>
                            <td>{{$noticia->fecha_publicacion}}</td>                            
                            <td><img src="{{ URL::to('/') }}{{$noticia->url_imagen}}" alt="" class="img w-25 mw-25"></td>
                            <td>
                                <a role="button" href="{{ url('admin/noticias/'.$noticia->id_noticia)}}" data-toggle="tooltip" title="Editar" data-placement="bottom" class="btn-shadow mr-auto btn btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a role="button" href="{{ url('admin/noticias/delete/'.$noticia->id_noticia)}}" data-toggle="tooltip" title="Borrar" data-placement="bottom" class="btn-shadow mr-auto btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <th scope="row">*</th>
                            <td>No hay noticias registradas</td>
                            <td>
                                <a role="button" href="{{ url('admin/noticias/new')}}" data-toggle="tooltip" title="Crear noticia" data-placement="bottom" class="btn-shadow mr-auto btn btn-success">
                                    <i class="fa fa-star"></i>
                                </a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                {!! $noticias->links() !!}
            </div>
        </div>
    </div>

</div>

@endsection