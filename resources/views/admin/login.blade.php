<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin - Login</title>
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href=".https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/login.css') }}">
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
</head>

<body class="d-flex flex-column">
    <main class="m-auto">
        <div class="container-fluid">
            <div class="card login-card">
                <div class="card-body">
                    <h2 class="login-card-title">Sign in</h2>
                    <form action="https://www.bootstrapdash.com/demo/login-templates-pro/login-9/#!">
                        <div class="form-group">
                            <label for="username" class="sr-only">Username</label>
                            <input type="text" name="username" id="username" class="form-control"
                                placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label for="password" class="sr-only">Password</label>
                            <input type="password" name="password" id="password" class="form-control"
                                placeholder="Password">
                        </div>
                        <div class="form-options-wrapper">
                            <div class="custom-control custom-checkbox login-card-check-box">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Remember Me</label>
                            </div>
                            <a href="https://www.bootstrapdash.com/demo/login-templates-pro/login-9/#!"
                                class="text-reset">Forgot Your Password?</a>
                        </div>
                        <!--<input name="login" id="login" class="btn login-btn" type="button" value="Login">-->
                        <a class="btn login-btn" role="button" href="{{ route("admin_home") }}">Login</a>
                    </form>
                    <p class="login-card-footer-text">Need an account? <a
                            href="https://www.bootstrapdash.com/demo/login-templates-pro/login-9/#!"
                            class="text-reset">Signup</a></p>
                </div>
            </div>
        </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>

</html>