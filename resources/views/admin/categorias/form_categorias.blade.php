@extends('templates/admin_template')

@php
    $title = 'Registrar';
    $descripcion = '';
    $id_categoria = '';
    if(isset($categoria)){
        $title = 'Actualizar';
        $descripcion = $categoria->categoria;
        $id_categoria = $categoria->id_categoria;
    }
@endphp

@section('title', 'Admin - '.$title.' categorias')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>{{$title}} categorías
                    <div class="page-title-subheading">Rellena el formulario
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a role="button" href="{{ url('admin/categorias') }}" data-toggle="tooltip" title="Regresar" data-placement="bottom"
                    class="btn-shadow mr-3 btn btn-dark">
                    <i class="fa fa-arrow-left"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="main-card mb-3 card w-50">
            <div class="card-body">
                <form class="" method='POST' action='{{ url('/admin/categorias') }}'>
                    {{ csrf_field() }}
                    <input type="hidden" name="id_categoria" value="{{$id_categoria}}">
                    <div class="position-relative form-group"><label for="categoria" class="">Descripcion</label><input
                            name="categoria" id="categoria" placeholder="..." type="text" value ="{{$descripcion}}"
                            class="form-control"></div>
                    <button class="mt-1 btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection