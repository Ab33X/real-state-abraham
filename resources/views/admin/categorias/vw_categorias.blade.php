@extends('templates/admin_template')

@section('title', 'Admin - Categorias')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Categorías
                    <div class="page-title-subheading">Administra las categorías para las propiedades aquí.
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        class="btn-shadow dropdown-toggle btn btn-dark">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-business-time fa-w-20"></i>
                        </span>
                        Acciones
                    </button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a href="{{ url('admin/categorias/new')}}" class="nav-link">
                                    <i class="nav-link-icon lnr-inbox"></i>
                                    <span>
                                        Nuevo
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="main-card mb-3 card w-100">
            <div class="card-body">
                <table class="mb-0 table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Descripcion</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($categorias->count() > 0)
                        @foreach ($categorias as $categoria)
                        <tr>
                            <th scope="row">{{$categoria->id_categoria}}</th>
                            <td>{{$categoria->categoria}}</td>
                            <td>
                                <a role="button" href="{{ url('admin/categorias/'.$categoria->id_categoria)}}" data-toggle="tooltip" title="Editar" data-placement="bottom" class="btn-shadow mr-auto btn btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a role="button" href="{{ url('admin/categorias/delete/'.$categoria->id_categoria)}}" data-toggle="tooltip" title="Borrar" data-placement="bottom" class="btn-shadow mr-auto btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <th scope="row">*</th>
                            <td>No hay categorías registradas</td>
                            <td>
                                <a role="button" href="{{ url('admin/categorias/new')}}" data-toggle="tooltip" title="Crear categoria" data-placement="bottom" class="btn-shadow mr-auto btn btn-success">
                                    <i class="fa fa-star"></i>
                                </a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection