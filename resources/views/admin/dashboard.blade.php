@extends('templates/admin_template')

@section('title', 'Admin - Welcome')

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-home icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Admin Dashboard
                    <div class="page-title-subheading">
                        Administra desde aquí todos los aspectos básicos para la página Real State Abraham. ;)
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="app-wrapper-footer">
    <div class="app-footer">
        <div class="app-footer__inner">
            <div class="app-footer-left">
            </div>
            <div class="app-footer-right">
            </div>
        </div>
    </div>
</div>
@endsection