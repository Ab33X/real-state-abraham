@extends('templates/admin_template')

@php
    $title = 'Registrar';
    $id_agente = '';
    $nombre = '';
    $email = '';
    $telefono = '';
    $celular = '';
    $experiencia = '';
    $imagen = '';
    $skype = '';
    $faceboook = '';
    $twitter = '';
    $instagram = '';
    $pinterest = '';
    if(isset($agente)){
        $title = 'Actualizar';
        $id_agente = $agente->id_agente;
        $nombre = $agente->nombre;
        $email = $agente->email;
        $telefono = $agente->telefono;
        $celular = $agente->celular;
        $experiencia = $agente->experiencia;
        $imagen = $agente->imagen;
        $skype = $agente->skype;
        $faceboook = $agente->faceboook;
        $twitter = $agente->twitter;
        $instagram = $agente->instagram;
        $pinterest = $agente->pinterest;
    }
@endphp

@section('title', 'Admin - '.$title.' agentes')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>{{$title}} agentes
                    <div class="page-title-subheading">Rellena el formulario
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a role="button" href="{{ url('admin/agentes') }}" data-toggle="tooltip" title="Regresar"
                    data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fa fa-arrow-left"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="main-card mb-3 card w-50">
            <div class="card-body">
                <form class="" method='POST' action='{{ url('/admin/agentes') }}' enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_agente" value="{{$id_agente}}">
                    <input type="hidden" name="imagen" value="{{$imagen}}">
                    <div class="position-relative form-group">
                        <label for="nombre" class="">Nombre del agente</label>
                        <input name="nombre" id="nombre" type="text" value="{{$nombre}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="email" class="">Email del agente</label>
                        <input name="email" id="email" type="email" value="{{$email}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="telefono" class="">Teléfono del agente</label>
                        <input name="telefono" id="telefono" type="number" value="{{$telefono}}" step="1" pattern="\d+" class="form-control" maxlength="10">
                    </div>
                    <div class="position-relative form-group">
                        <label for="celular" class="">Celular del agente</label>
                        <input name="celular" id="celular" type="number" value="{{$celular}}" step="1" pattern="\d+" class="form-control" maxlength="10">
                    </div>
                    <div class="position-relative form-group">
                        <label for="experiencia" class="">Experiencia del agente</label>
                        <input name="experiencia" id="experiencia" type="text" value="{{$experiencia}}" class="form-control">
                    </div>                     
                    <div class="position-relative form-group">
                        <label for="fileImage" class="">Imagen del agente</label>
                        <input name="fileImage" id="fileImage" type="file" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="skype" class="">Skype del agente</label>
                        <input name="skype" id="skype" type="url" value="{{$skype}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="faceboook" class="">Facebook del agente</label>
                        <input name="faceboook" id="faceboook" type="url" value="{{$faceboook}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="twitter" class="">Twitter del agente</label>
                        <input name="twitter" id="twitter" type="url" value="{{$twitter}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="instagram" class="">Instagram del agente</label>
                        <input name="instagram" id="instagram" type="url" value="{{$instagram}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="pinterest" class="">Pinterest del agente</label>
                        <input name="pinterest" id="pinterest" type="url" value="{{$pinterest}}" class="form-control">
                    </div>
                    <button class="mt-1 btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection