@extends('templates/admin_template')

@section('title', 'Admin - Agentes')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Agentes
                    <div class="page-title-subheading">Administra los agentes que serán encargados de las distintas propiedades.
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        class="btn-shadow dropdown-toggle btn btn-dark">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-business-time fa-w-20"></i>
                        </span>
                        Acciones
                    </button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a href="{{ url('admin/agentes/new')}}" class="nav-link">
                                    <i class="nav-link-icon lnr-inbox"></i>
                                    <span>
                                        Nuevo
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="main-card mb-3 card w-100">
            <div class="card-body">
                <table class="mb-0 table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Teléfono</th>
                            <th>Celular</th>
                            <th>Experiencia</th>
                            <th>Imagen</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($agentes->count() > 0)
                        @foreach ($agentes as $agente)
                        <tr>
                            <th scope="row">{{$agente->id_agente}}</th>
                            <td>{{$agente->nombre}}</td>
                            <td>{{$agente->email}}</td>
                            <td>{{$agente->telefono}}</td>
                            <td>{{$agente->celular}}</td>
                            <td>{{$agente->experiencia}}</td>
                            <td><img src="{{ URL::to('/') }}{{$agente->url_imagen}}" alt="" class="img w-25 mw-25"></td>
                            <td>
                                <a role="button" href="{{ url('admin/agentes/'.$agente->id_agente)}}" data-toggle="tooltip" title="Editar" data-placement="bottom" class="btn-shadow mr-auto btn btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a role="button" href="{{ url('admin/agentes/delete/'.$agente->id_agente)}}" data-toggle="tooltip" title="Borrar" data-placement="bottom" class="btn-shadow mr-auto btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <th scope="row">*</th>
                            <td>No hay</td>
                            <td>agentes</td>
                            <td>registrados</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <a role="button" href="{{ url('admin/agentes/new')}}" data-toggle="tooltip" title="Registrar agente" data-placement="bottom" class="btn-shadow mr-auto btn btn-success">
                                    <i class="fa fa-star"></i>
                                </a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection