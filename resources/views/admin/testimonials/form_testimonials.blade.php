@extends('templates/admin_template')

@php
    $module_title = 'Registrar';
    $id_testimonial = '';
    $title = '';
    $comment = '';
    $imagen = '';
    if(isset($testimonial)){
        $module_title = 'Actualizar';        
        $id_testimonial = $testimonial->id_testimonial;
        $title = $testimonial->title;
        $comment = $testimonial->comment;
        $imagen = $testimonial->imagen;

    }
@endphp

@section('title', 'Admin - '.$title.' testimonials')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>{{$module_title}} testimonials
                    <div class="page-title-subheading">Rellena el formulario
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a role="button" href="{{ url('admin/testimonials') }}" data-toggle="tooltip" title="Regresar"
                    data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fa fa-arrow-left"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="main-card mb-3 card w-50">
            <div class="card-body">
                <form class="" method='POST' action='{{ url('/admin/testimonials') }}' enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_testimonial" value="{{$id_testimonial}}">
                    <input type="hidden" name="imagen" value="{{$imagen}}">
                    <div class="position-relative form-group">
                        <label for="title" class="">Titulo del testimonial</label>
                        <input name="title" id="title" type="text" value="{{$title}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="comment" class="">Comentario del testimonial</label>
                        <input name="comment" id="comment" type="text" value="{{$comment}}" class="form-control">
                    </div>                   
                    <div class="position-relative form-group">
                        <label for="fileImage" class="">Imagen del testimonial</label>
                        <input name="fileImage" id="fileImage" type="file" class="form-control">
                    </div>
                    <button class="mt-1 btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection