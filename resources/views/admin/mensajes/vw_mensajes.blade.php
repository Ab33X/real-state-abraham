@extends('templates/admin_template')

@section('title', 'Admin - Mensajes')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Mensajes
                    <div class="page-title-subheading">Visualiza desde aquí todos los mensajes que han recibido las propiedades.
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="main-card mb-3 card w-100">
            <div class="card-body">
                <table class="mb-0 table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Comentario</th>
                            <th>Agente</th>
                            <th>Propiedad</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($mensajes->count() > 0)
                        @foreach ($mensajes as $mensaje)
                        <tr>
                            @php
                                $agente = $mensaje->agente;
                                $propiedad = $mensaje->propiedad;
                            @endphp
                            <th scope="row">{{$mensaje->id_mensaje}}</th>
                            <td>{{$mensaje->nombre}}</td>
                            <td>{{$mensaje->correo}}</td>
                            <td>{{$mensaje->comentario}}</td>
                            <td>{{$agente->nombre}}</td>
                            <td>{{$propiedad->titulo}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <th scope="row">*</th>
                            <td>No hay mensajes registrados</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection