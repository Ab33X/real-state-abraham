@extends('templates/admin_template')

@php
$title = 'Registrar';
$id_propiedad = '';
$titulo = '';
$descripcion = '';
$precio = '';
$localizacion = '';
$area = '';
$cuartos = '';
$banios = '';
$garages = '';
$id_categoria = '';
$id_agente = '';
if (isset($propiedad)) {
    $title = 'Actualizar';
    $id_propiedad = $propiedad->id_propiedad;
    $titulo = $propiedad->titulo;
    $descripcion = $propiedad->descripcion;
    $precio = $propiedad->precio;
    $localizacion = $propiedad->localizacion;
    $area = $propiedad->area;
    $cuartos = $propiedad->cuartos;
    $banios = $propiedad->banios;
    $garages = $propiedad->garages;
    $id_categoria = $propiedad->id_categoria;
    $id_agente = $propiedad->id_agente;
}
@endphp

@section('title', 'Admin - ' . $title . ' propiedades')

@section('content')

    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>{{ $title }} propiedades
                        <div class="page-title-subheading">Rellena el formulario
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a role="button" href="{{ url('admin/propiedades') }}" data-toggle="tooltip" title="Regresar"
                        data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="main-card mb-3 card w-50">
                <div class="card-body">
                    <form class="" method='POST' action='{{ url('/admin/propiedades') }}'>
                        {{ csrf_field() }}
                        <input type="hidden" name="id_propiedad" value="{{ $id_propiedad }}">
                        <div class="position-relative form-group"><label for="titulo" class="">Titulo</label><input
                                name="titulo" id="titulo" placeholder="..." type="text" value="{{ $titulo }}"
                                class="form-control"></div>
                        <div class="position-relative form-group"><label for="descripcion"
                                class="">Descripcion</label><input name="descripcion" id="descripcion" placeholder="..."
                                type="text" value="{{ $descripcion }}" class="form-control"></div>
                        <div class="position-relative form-group"><label for="id_agente" class="">Agente</label><select
                                name="id_agente" id="id_agente" class="form-control">
                                @if ($agentes->count() > 0)
                                    <option value="">Ninguno</option>
                                    @foreach ($agentes as $agente)

                                        <option value="{{ $agente->id_agente }}" @if ($agente->id_agente == $id_agente) selected @endif>{{ $agente->nombre }}</option>
                                    @endforeach
                                @else
                                    <option value="">No hay agentes</option>
                                @endif
                            </select></div>
                        <div class="position-relative form-group"><label for="id_categoria"
                                class="">Categoria</label><select name="id_categoria" id="id_categoria"
                                class="form-control">
                                @if ($categorias->count() > 0)
                                    <option value="">Ninguna</option>
                                    @foreach ($categorias as $categoria)

                                        <option value="{{ $categoria->id_categoria }}" @if ($categoria->id_categoria == $id_categoria) selected @endif>{{ $categoria->categoria }}</option>
                                    @endforeach
                                @else
                                    <option value="">No hay categorias</option>
                                @endif
                            </select></div>

                        <div class="position-relative form-group"><label for="precio" class="">Precio</label><input
                                name="precio" id="precio" placeholder="MXN" type="number" value="{{ $precio }}"
                                step="any" class="form-control"></div>

                        <div class="position-relative form-group"><label for="localizacion"
                                class="">Localizacion</label><input name="localizacion" id="localizacion" placeholder="..."
                                type="text" value="{{ $localizacion }}" class="form-control"></div>

                        <div class="position-relative form-group"><label for="area" class="">Area</label><input name="area"
                                id="area" placeholder="..." type="text" value="{{ $area }}" class="form-control"
                                maxlength="50"></div>

                        <div class="position-relative form-group"><label for="cuartos" class="">Cuartos</label><input
                                name="cuartos" id="cuartos" placeholder="..." pattern="\d+" type="number" step="1"
                                value="{{ $cuartos }}" class="form-control"></div>

                        <div class="position-relative form-group"><label for="banios" class="">Baños</label><input
                                name="banios" id="banios" placeholder="..." pattern="\d+" type="number" step="1"
                                value="{{ $banios }}" class="form-control"></div>

                        <div class="position-relative form-group"><label for="garages" class="">Garages</label><input
                                name="garages" id="garages" placeholder="..." pattern="\d+" type="number" step="1"
                                value="{{ $garages }}" class="form-control"></div>

                        <button class="mt-1 btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
