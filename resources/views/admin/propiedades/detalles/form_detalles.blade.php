@extends('templates/admin_template')

@php
$title = 'Registrar';
$id_detalle_propiedad = '';
$id_propiedad = '';
$descripcion = '';
if (isset($detalle)) {
    $title = 'Actualizar';
    $id_detalle_propiedad = $detalle->id_detalle_propiedad;
    $id_propiedad = $detalle->id_propiedad;
    $descripcion = $detalle->descripcion;
} else {
    $id_propiedad = $propiedad->id_propiedad;
}
@endphp

@section('title', 'Admin - ' . $title . ' detalles de propiedad')

@section('content')

    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>{{ $title }} detalles de la propiedad [{{ $propiedad->titulo}}]
                        <div class="page-title-subheading">Rellena el formulario
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a role="button" href="{{ url('admin/propiedades/detalles/'.$propiedad->id_propiedad) }}" data-toggle="tooltip" title="Regresar"
                        data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="main-card mb-3 card w-50">
                <div class="card-body">
                    <form class="" method='POST' action='{{ url('/admin/propiedades/detalles') }}'>
                        {{ csrf_field() }}
                        <input type="hidden" name="id_detalle_propiedad" value="{{ $id_detalle_propiedad }}">
                        <input type="hidden" name="id_propiedad" value="{{ $id_propiedad }}">
                        <div class="position-relative form-group"><label for="descripcion"
                                class="">Descripcion</label><input name="descripcion" id="descripcion" placeholder="..."
                                type="text" value="{{ $descripcion }}" class="form-control"></div>                 

                        <button class="mt-1 btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
