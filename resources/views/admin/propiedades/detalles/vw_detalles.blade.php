@extends('templates/admin_template')

@section('title', 'Admin - Detalles de la propiedad')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Detalles de [{{ $propiedad->titulo }}]
                    <div class="page-title-subheading">Administra aqui los detalles técnicos, físicos o estéticos que mostrará el sistema para esta propiedad.
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    
                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        class="btn-shadow dropdown-toggle btn btn-dark">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-business-time fa-w-20"></i>
                        </span>
                        Acciones
                    </button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a href="{{ url('admin/propiedades/detalles/new/'.$propiedad->id_propiedad)}}" class="nav-link">
                                    <i class="nav-link-icon lnr-inbox"></i>
                                    <span>
                                        Nuevo
                                    </span>
                                </a>
                                <a href="{{ url('admin/propiedades')}}" class="nav-link">
                                    <i class="nav-link-icon lnr-inbox fa fa-arrow-left"></i>
                                    <span>
                                        Regresar
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="main-card mb-3 card w-100">
            <div class="card-body">
                <table class="mb-0 table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Descripcion</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($detalles->count() > 0)
                        @foreach ($detalles as $detalle)
                        <tr>
                            <th scope="row">{{$detalle->id_detalle_propiedad}}</th>
                            <td>{{$detalle->descripcion}}</td>
                            <td>
                                <a role="button" href="{{ url('admin/propiedades/detalles/edit/'.$detalle->id_detalle_propiedad)}}" data-toggle="tooltip" title="Editar" data-placement="bottom" class="btn-shadow mr-auto btn btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a role="button" href="{{ url('admin/propiedades/detalles/delete/'.$detalle->id_detalle_propiedad)}}" data-toggle="tooltip" title="Borrar" data-placement="bottom" class="btn-shadow mr-auto btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <th scope="row">*</th>
                            <td>No hay detalles registrados para esta propiedad</td>
                            <td>
                                <a role="button" href="{{ url('admin/propiedades/detalles/new/'.$propiedad->id_propiedad)}}" data-toggle="tooltip" title="Crear detalle de propiedad" data-placement="bottom" class="btn-shadow mr-auto btn btn-success">
                                    <i class="fa fa-star"></i>
                                </a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                {!! $detalles->links() !!}
            </div>
        </div>
    </div>

</div>

@endsection