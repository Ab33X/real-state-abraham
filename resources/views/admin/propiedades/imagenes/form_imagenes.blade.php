@extends('templates/admin_template')

@php
    $title = 'Registrar';
    $id_imagen_propiedad = '';
    $id_propiedad = '';
    $titulo_imagen = '';
    $nombre_archivo = '';
    $extension = '';
    if(isset($propiedad)){
        $title = 'Actualizar';        
        $id_imagen_propiedad = $propiedad->id_imagen_propiedad;
        $id_propiedad = $propiedad->id_propiedad;
        $titulo_imagen = $propiedad->titulo_imagen;
        $nombre_archivo = $propiedad->nombre_archivo;
        $extension = $propiedad->extension;
    } else {
        $id_propiedad = $propiedad->id_propiedad;
    }
@endphp

@section('title', 'Admin - '.$title.' imagenes de propiedad')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>{{$title}} imagenes de la propiedad [{{ $propiedad->titulo}}]
                    <div class="page-title-subheading">Rellena el formulario
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <a role="button" href="{{ url('admin/propiedades/noticias/'.$propiedad->id_propiedad) }}" data-toggle="tooltip" title="Regresar"
                    data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fa fa-arrow-left"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="main-card mb-3 card w-50">
            <div class="card-body">
                <form class="" method='POST' action='{{ url('/admin/propiedades/imagenes') }}' enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_imagen_propiedad" value="{{$id_imagen_propiedad}}">
                    <input type="hidden" name="id_propiedad" value="{{$id_propiedad}}">
                    <input type="hidden" name="nombre_archivo" value="{{$nombre_archivo}}">
                    <input type="hidden" name="extension" value="{{$extension}}">
                    <div class="position-relative form-group">
                        <label for="titulo_imagen" class="">Titulo de la imagen</label>
                        <input name="titulo_imagen" id="titulo_imagen" type="text" value="{{$titulo_imagen}}" class="form-control">
                    </div>                    
                    <div class="position-relative form-group">
                        <label for="fileImage" class="">Imagen</label>
                        <input name="fileImage" id="fileImage" type="file" class="form-control">
                    </div>
                    <button class="mt-1 btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection