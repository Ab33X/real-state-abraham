@extends('templates/admin_template')

@section('title', 'Admin - Imagenes de la propiedad')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Imagenes de [{{ $propiedad->titulo }}]
                    <div class="page-title-subheading">Administra aqui las imagenes que mostrará el sistema para esta propiedad.
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    
                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        class="btn-shadow dropdown-toggle btn btn-dark">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-business-time fa-w-20"></i>
                        </span>
                        Acciones
                    </button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a href="{{ url('admin/propiedades/imagenes/new/'.$propiedad->id_propiedad)}}" class="nav-link">
                                    <i class="nav-link-icon lnr-inbox"></i>
                                    <span>
                                        Nuevo
                                    </span>
                                </a>
                                <a href="{{ url('admin/propiedades')}}" class="nav-link">
                                    <i class="nav-link-icon lnr-inbox fa fa-arrow-left"></i>
                                    <span>
                                        Regresar
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">          
            @if ($imagenes->count() > 0)
                @foreach ($imagenes as $imagen)
                    <div class="col-md-4">  
                        <div class="main-card mb-3 card"><img width="100%" src="{{ URL::to('/') }}{{ $imagen->url_imagen }}" alt="{{ $imagen->titulo_imagen }}" class="card-img-top">
                            <div class="card-body"><h5 class="card-title">{{ $imagen->titulo_imagen}}</h5><h6 class="card-subtitle">{{ $imagen->extension }}</h6>                            
                                <a role="button" href="{{ url('admin/propiedades/imagenes/edit/'.$imagen->id_imagen_propiedad)}}" data-toggle="tooltip" title="Editar" data-placement="bottom" class="btn-shadow mr-auto btn btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a role="button" href="{{ url('admin/propiedades/imagenes/delete/'.$imagen->id_imagen_propiedad)}}" data-toggle="tooltip" title="Borrar" data-placement="bottom" class="btn-shadow mr-auto btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-md-4">  
                    <div class="main-card mb-3 card"><img width="100%" src="{{ URL::to('/') }}/image-not-found.png" alt="No hay imagenes" class="card-img-top">
                        <div class="card-body"><h5 class="card-title">No hay imagenes</h5>Agrega una ahora
                            <a role="button" href="{{ url('admin/propiedades/imagenes/new/'.$propiedad->id_propiedad)}}" data-toggle="tooltip" title="Agregar imagen de propiedad" data-placement="bottom" class="btn-shadow mr-auto btn btn-success">
                                <i class="fa fa-star"></i>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
            {!! $imagenes->links() !!}
    </div>

</div>

@endsection