@extends('templates/admin_template')

@section('title', 'Admin - Propiedades')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Propiedades
                    <div class="page-title-subheading">Administra aqui las propiedades que mostrará el sistema.
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    
                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        class="btn-shadow dropdown-toggle btn btn-dark">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-business-time fa-w-20"></i>
                        </span>
                        Acciones
                    </button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a href="{{ url('admin/propiedades/new')}}" class="nav-link">
                                    <i class="nav-link-icon lnr-inbox"></i>
                                    <span>
                                        Nuevo
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="main-card mb-3 card w-100">
            <div class="card-body">
                <table class="mb-0 table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Titulo</th>
                            <th>Descripcion</th>
                            <th>Precio</th>
                            <th>Localizacion</th>
                            <th>Agente</th>
                            <th>Categoria</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($propiedades->count() > 0)
                        @foreach ($propiedades as $propiedad)
                        <tr>
                            @php
                                $agente = $propiedad->agente;
                                $categoria = $propiedad->categoria;
                            @endphp
                            <th scope="row">{{$propiedad->id_propiedad}}</th>
                            <td>{{$propiedad->titulo}}</td>
                            <td>{{$propiedad->descripcion}}</td>
                            <td>{{$propiedad->precio_formato}}</td>
                            <td>{{$propiedad->localizacion}}</td>
                            <td>{{$agente->nombre}}</td>
                            <td>{{$categoria->categoria}}</td>
                            <td>
                                <a role="button" href="{{ url('admin/propiedades/'.$propiedad->id_propiedad)}}" data-toggle="tooltip" title="Editar" data-placement="bottom" class="btn-shadow mr-auto btn btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a role="button" href="{{ url('admin/propiedades/delete/'.$propiedad->id_propiedad)}}" data-toggle="tooltip" title="Borrar" data-placement="bottom" class="btn-shadow mr-auto btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                                <a role="button" href="{{ url('admin/propiedades/detalles/'.$propiedad->id_propiedad)}}" data-toggle="tooltip" title="Editar detalles" data-placement="bottom" class="btn-shadow mr-auto btn btn-success">
                                    <i class="fa fa-list"></i>
                                </a>
                                <a role="button" href="{{ url('admin/propiedades/imagenes/'.$propiedad->id_propiedad)}}" data-toggle="tooltip" title="Editar imagenes" data-placement="bottom" class="btn-shadow mr-auto btn btn-success">
                                    <i class="fa fa-images"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <th scope="row">*</th>
                            <td>No hay propiedades registradas</td>
                            <td>
                                <a role="button" href="{{ url('admin/propiedades/new')}}" data-toggle="tooltip" title="Crear propiedad" data-placement="bottom" class="btn-shadow mr-auto btn btn-success">
                                    <i class="fa fa-star"></i>
                                </a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                {!! $propiedades->links() !!}
            </div>
        </div>
    </div>

</div>

@endsection