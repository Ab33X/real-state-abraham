@extends('templates/template')

@section('title', 'Real State - Properties')

@section('content')

<!-- ======= Agents Section ======= -->
<section class="section-agents section-t8">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-wrap d-flex justify-content-between">
                    <div class="title-box">
                        <h2 class="title-a">All properties</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        @if ($propiedades->count() > 0)
            @foreach ($propiedades as $propiedad)
            <div class="col-md-4">
                <div class="card-box-d">
                    <div class="card-img-d">
                        <img src="{{ URL::to('/') }}{{ $propiedad->last_image }}" alt="" class="img-d img-fluid">
                    </div>
                    <div class="card-overlay card-overlay-hover">
                        <div class="card-header-d">
                            <div class="card-title-d align-self-center">
                                <h3 class="title-d">
                                    <a href="{{ route('detail', ['slug' => $propiedad->slug]) }}" class="link-two">{{ $propiedad->titulo }}</a>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body-d">
                            <p class="content-d color-text-a">
                                {{ $propiedad->localizacion}}
                            </p>
                            <div class="info-agents color-a">
                                <p>
                                    <strong>$</strong> {{ $propiedad->precio_formato}}
                                </p>
                            </div>
                        </div>
                        <div class="card-footer-d">
                            <div class="socials-footer d-flex justify-content-center">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="{{ route('detail', ['slug' => $propiedad->slug]) }}" class="link-one">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            {!! $propiedades->links() !!}
        @endif
        </div>
    </div>
</section><!-- End Agents Section -->

@endsection