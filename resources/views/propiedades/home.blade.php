@extends('templates/template')

@section('title', 'Real State - Home')

@section('carousel')
    @php
        $best_props = getBestProps();        
    @endphp
    <!-- ======= Intro Section ======= -->
    <div class="intro intro-carousel">
        <div id="carousel" class="owl-carousel owl-theme">            
        @if ($best_props->count() > 0)
            @foreach ($best_props as $propiedad)
            <div class="carousel-item-a intro-item bg-image" style="background-image: url({{ URL::to('/') }}{{ $propiedad->last_image }})">
                <div class="overlay overlay-a"></div>
                <div class="intro-content display-table">
                    <div class="table-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="intro-body">
                                        <p class="intro-title-top"> {{ $propiedad->localizacion}}
                                        </p>
                                        <h1 class="intro-title mb-4">
                                            <span class="color-b">{{ $propiedad->id_propiedad }} </span> {{ $propiedad->titulo }}
                                        </h1>
                                        <p class="intro-subtitle intro-price">
                                            <a href="{{ route('detail', ['slug' => $propiedad->slug]) }}"><span class="price-a">rent | $ {{ $propiedad->precio_formato}}</span></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
        </div>
    </div>
    <!-- End Intro Section -->
@endsection

@section('content')

    <!-- ======= Agents Section ======= -->
    <section class="section-agents section-t8">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-wrap d-flex justify-content-between">
                        <div class="title-box">
                            <h2 class="title-a">Best Agents</h2>
                        </div>
                        <div class="title-link">
                            <a href="{{ route('agentes') }}">All Agents
                                <span class="ion-ios-arrow-forward"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            @php
                $agentes = getBestAgents();
            @endphp
            @if ($agentes->count() > 0)
                @foreach ($agentes as $agente)
                <div class="col-md-4">
                    <div class="card-box-d">
                        <div class="card-img-d">
                            <img src="{{ URL::to('/') }}{{ $agente->url_imagen }}" alt="" class="img-d img-fluid">
                        </div>
                        <div class="card-overlay card-overlay-hover">
                            <div class="card-header-d">
                                <div class="card-title-d align-self-center">
                                    <h3 class="title-d">
                                        <a href="{{ route('agentes') }}" class="link-two">{{ $agente->nombre }}</a>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body-d">
                                <p class="content-d color-text-a">
                                    {{ $agente->experiencia }}
                                </p>
                                <div class="info-agents color-a">
                                    <p>
                                        <strong>Phone: </strong> {{ $agente->celular }}
                                    </p>
                                    <p>
                                        <strong>Email: </strong> {{ $agente->email }}
                                    </p>
                                </div>
                            </div>
                            <div class="card-footer-d">
                                <div class="socials-footer d-flex justify-content-center">
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <a href="{{ $agente->facebook }}" class="link-one">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="{{ $agente->twitter }}" class="link-one">
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="{{ $agente->instagram }}" class="link-one">
                                                <i class="fa fa-instagram" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="{{ $agente->pinterest }}" class="link-one">
                                                <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="{{ $agente->skype }}" class="link-one">
                                                <i class="fa fa-dribbble" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
            </div>
        </div>
    </section><!-- End Agents Section -->

    <!-- ======= Latest News Section ======= -->
    <section class="section-news section-t8">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-wrap d-flex justify-content-between">
                        <div class="title-box">
                            <h2 class="title-a">Latest News</h2>
                        </div>
                        <div class="title-link">
                            <a href="{{ route('blog') }}">All News
                                <span class="ion-ios-arrow-forward"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="new-carousel" class="owl-carousel owl-theme">                
                @php
                    $noticias = getUltimasNoticias();
                @endphp
                @if ($noticias->count() > 0)
                    @foreach ($noticias as $noticia)
                    <div class="carousel-item-c">
                        <div class="card-box-b card-shadow news-box">
                            <div class="img-box-b">
                                <img src="{{ URL::to('/') }}{{ $noticia->url_imagen }}" alt="" class="img-b img-fluid">
                            </div>
                            <div class="card-overlay">
                                <div class="card-header-b">
                                    <div class="card-category-b">
                                        <a href="#" class="category-b">{{ $noticia->titulo }}</a>
                                    </div>
                                    <div class="card-title-b">
                                        <h2 class="title-2">
                                            <a href="blog-single.html">{{ $noticia->subtitulo }}</a>
                                        </h2>
                                    </div>
                                    <div class="card-date">
                                        <span class="date-b">{{ $noticia->fecha_publicacion }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section><!-- End Latest News Section -->

@endsection
