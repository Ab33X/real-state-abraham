@extends('templates/template')

@section('title', 'Real State - Agents')

@section('content')

<!-- ======= Agents Section ======= -->
<section class="section-agents section-t8">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-wrap d-flex justify-content-between">
                    <div class="title-box">
                        <h2 class="title-a">All agents</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        @if ($agentes->count() > 0)
            @foreach ($agentes as $agente)
            <div class="col-md-4">
                <div class="card-box-d">
                    <div class="card-img-d">
                        <img src="{{ URL::to('/') }}{{ $agente->url_imagen }}" alt="" class="img-d img-fluid">
                    </div>
                    <div class="card-overlay card-overlay-hover">
                        <div class="card-header-d">
                            <div class="card-title-d align-self-center">
                                <h3 class="title-d">
                                    <a href="{{ route('agentes') }}" class="link-two">{{ $agente->nombre }}</a>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body-d">
                            <p class="content-d color-text-a">
                                {{ $agente->experiencia }}
                            </p>
                            <div class="info-agents color-a">
                                <p>
                                    <strong>Phone: </strong> {{ $agente->celular }}
                                </p>
                                <p>
                                    <strong>Email: </strong> {{ $agente->email }}
                                </p>
                            </div>
                        </div>
                        <div class="card-footer-d">
                            <div class="socials-footer d-flex justify-content-center">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="{{ $agente->facebook }}" class="link-one">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{ $agente->twitter }}" class="link-one">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{ $agente->instagram }}" class="link-one">
                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{ $agente->pinterest }}" class="link-one">
                                            <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{ $agente->skype }}" class="link-one">
                                            <i class="fa fa-dribbble" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            {!! $agentes->links() !!}
        @endif
        </div>
    </div>
</section><!-- End Agents Section -->

@endsection