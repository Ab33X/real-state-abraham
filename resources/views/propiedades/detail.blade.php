    @extends('templates/template')

    @section('title', 'Real State - ' . $propiedad->titulo)

    @section('content')
        <!-- ======= Intro Single ======= -->
        <section class="intro-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-8">
                        <div class="title-single-box">
                            <h1 class="title-single">{{ $propiedad->titulo }}</h1>
                            <span class="color-text-a">{{ $propiedad->localizacion }}</span>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{ route('propiedades') }}">Properties</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    {{ $propiedad->titulo }}
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section><!-- End Intro Single-->

        <!-- ======= Property Single ======= -->
        <section class="property-single nav-arrow-b">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="property-single-carousel" class="owl-carousel owl-arrow gallery-property">
                            @if ($propiedad->imagenes_propiedades->count() > 0)
                                @foreach ($propiedad->imagenes_propiedades as $imagen)
                                    <div class="carousel-item-b">
                                        <img src="{{ URL::to('/') }}{{ $imagen->url_imagen }}"
                                            alt="{{ $imagen->titulo_imagen }}">
                                    </div>
                                @endforeach
                            @else
                                <div class="carousel-item-b">
                                    <img src="{{ URL::to('/') }}/image-not-found.png" alt="No hay imagenes de esta propiedad">
                                </div>
                            @endif
                        </div>
                        <div class="row justify-content-between">
                            <div class="col-md-5 col-lg-4">
                                <div class="property-price d-flex justify-content-center foo">
                                    <div class="card-header-c d-flex">
                                        <div class="card-box-ico">
                                            <span class="ion-money">$</span>
                                        </div>
                                        <div class="card-title-c align-self-center">
                                            <h5 class="title-c">{{ $propiedad->precio_formato }}</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="property-summary">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="title-box-d section-t4">
                                                <h3 class="title-d">Quick Summary</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="summary-list">
                                        <ul class="list">
                                            <li class="d-flex justify-content-between">
                                                <strong>Property ID:</strong>
                                                <span>{{ $propiedad->id_propiedad }}</span>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <strong>Location:</strong>
                                                <span>{{ $propiedad->localizacion }}</span>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <strong>Property Type:</strong>
                                                <span>{{ $propiedad->categoria->categoria }}</span>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <strong>Status:</strong>
                                                <!-- REPLACE WITH STATUS AND ADD TO THE TABLE -->
                                                <span>Sale</span>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <strong>Area:</strong>
                                                <span>{{ $propiedad->area }}
                                                </span>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <strong>Beds:</strong>
                                                <span>{{ $propiedad->cuartos }}</span>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <strong>Baths:</strong>
                                                <span>{{ $propiedad->banios }}</span>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <strong>Garage:</strong>
                                                <span>{{ $propiedad->garages }}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-7 section-md-t3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="title-box-d">
                                            <h3 class="title-d">Property Description</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="property-description">
                                    <p class="description color-text-a">
                                        {{ $propiedad->descripcion }}
                                    </p>
                                </div>



                                <div class="row section-t3">
                                    <div class="col-sm-12">
                                        <div class="title-box-d">
                                            <h3 class="title-d">Amenities</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="amenities-list color-text-a">
                                    <ul class="list-a no-margin">
                                        @if ($propiedad->detalles_propiedades->count() > 0)
                                            @foreach ($propiedad->detalles_propiedades as $detalle)
                                                <li>{{ $detalle->descripcion }}</li>
                                            @endforeach
                                        @else
                                            <li>No description</li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row section-t3">
                            <div class="col-sm-12">
                                <div class="title-box-d">
                                    <h3 class="title-d">Contact Agent</h3>
                                </div>
                            </div>
                        </div>
                        @php
                            $agente = $propiedad->agente;
                        @endphp
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <img src="{{ URL::to('/') }}{{$agente->url_imagen}}" alt="" class="img-fluid">
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <div class="property-agent">
                                    <h4 class="title-agent">{{$agente->nombre}}</h4>
                                    <p class="color-text-a">
                                        {{$agente->experiencia}}
                                    </p>
                                    <ul class="list-unstyled">
                                        <li class="d-flex justify-content-between">
                                            <strong>Phone:</strong>
                                            <span class="color-text-a">{{$agente->telefono}}</span>
                                        </li>
                                        <li class="d-flex justify-content-between">
                                            <strong>Mobile:</strong>
                                            <span class="color-text-a">{{$agente->celular}}</span>
                                        </li>
                                        <li class="d-flex justify-content-between">
                                            <strong>Email:</strong>
                                            <span class="color-text-a">{{$agente->email}}</span>
                                        </li>
                                        <li class="d-flex justify-content-between">
                                            <strong>Skype:</strong>
                                            <span class="color-text-a">{{$agente->skype}}</span>
                                        </li>
                                    </ul>
                                    <div class="socials-a">
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a href="{{$agente->faceboook}}" target="_blank">
                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="{{$agente->twitter}}" target="_blank">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="{{$agente->instagram}}" target="_blank">
                                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="{{$agente->pinterest}}" target="_blank">
                                                    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-4">
                                <div class="property-contact">
                                    <form class="form-a" method="POST" action="{{ url('/mensajes') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id_propiedad" value="{{ $propiedad->id_propiedad }}">                                
                                        <input type="hidden" name="id_agente" value="{{ $agente->id_agente }}">
                                        <div class="row">
                                            <div class="col-md-12 mb-1">
                                                <div class="form-group">
                                                    <input type="text" class="form-control form-control-lg form-control-a"
                                                        id="nombre" placeholder="Name *" name="nombre" required>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mb-1">
                                                <div class="form-group">
                                                    <input type="email" class="form-control form-control-lg form-control-a"
                                                        id="correo" name="correo" placeholder="Email *" required>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mb-1">
                                                <div class="form-group">
                                                    <textarea id="comentario" class="form-control" placeholder="Comment *"
                                                        name="comentario" cols="45" rows="8" required></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-a">Send Message</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End Property Single-->
    @endsection
