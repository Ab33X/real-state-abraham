@extends('templates/template')

@section('title', 'Real State - Blog')

@section('content')

<!-- ======= Agents Section ======= -->
<section class="section-agents section-t8">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-wrap d-flex justify-content-between">
                    <div class="title-box">
                        <h2 class="title-a">All news</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        @if ($noticias->count() > 0)
            @foreach ($noticias as $noticia)
            <div class="col-md-4">
                <div class="card-box-d">
                    <div class="card-img-d">
                        <img src="{{ URL::to('/') }}{{ $noticia->url_imagen }}" alt="" class="img-d img-fluid">
                    </div>
                    <div class="card-overlay card-overlay-hover">
                        <div class="card-header-d">
                            <div class="card-title-d align-self-center">
                                <h3 class="title-d">
                                    {{ $noticia->titulo }}
                                </h3>
                            </div>
                        </div>
                        <div class="card-body-d">
                            <p class="content-d color-text-a">
                                {{ $noticia->subtitulo}}
                            </p>
                            <div class="info-agents color-a">
                                <p>
                                    <strong>-</strong> {{ $noticia->fecha_publicacion}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            {!! $noticias->links() !!}
        @endif
        </div>
    </div>
</section><!-- End Agents Section -->

@endsection