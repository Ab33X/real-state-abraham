<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PropiedadesController;
use App\Http\Controllers\CategoriasController;
use App\Http\Controllers\AgentesController;
use App\Http\Controllers\DetallesPropiedadesController;
use App\Http\Controllers\ImagenesPropiedadesController;
use App\Http\Controllers\MensajesController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NoticiasController;
use App\Http\Controllers\TestimonialsController;
use App\Mail\Mailer;

// Real

Route::get('/', function () {
    return view('propiedades/home');
})->name('home');

Route::get('/about', [TestimonialsController::class, 'getAbout'])->name('about');

Route::get('/blog', [NoticiasController::class, 'getBlog'])->name("blog");

Route::get('/propiedades', [PropiedadesController::class, 'getProps'])->name('propiedades');

Route::get('/agents', [AgentesController::class, 'getEnrollment'])->name('agentes');

Route::get('/detail/{slug}', [PropiedadesController::class, 'detailPropiedad'])->name('detail');

// Mensajes

Route::post('/mensajes', [MensajesController::class, 'saveMensaje']);

// Dashboard

Route::prefix('admin')->group(function () {

    Route::get('', [UserController::class, 'index'])->name('admin_login');

    Route::get('/dashboard', function () {
        return view('admin/dashboard');
    })->name('admin_home');

    // Propiedades

    Route::get('/mensajes', [MensajesController::class, 'getMensajes']);

    Route::get('/propiedades', [PropiedadesController::class, 'getPropiedades'])->name('propiedades_home');

    Route::get('/propiedades/new', [PropiedadesController::class, 'newPropiedad']);

    Route::get('/propiedades/{id}', [PropiedadesController::class, 'editPropiedad']);

    Route::post('/propiedades', [PropiedadesController::class, 'customSavePropiedad']);

    Route::get('/propiedades/delete/{id}', [PropiedadesController::class, 'customDeletePropiedad']);

    // Detalles de propiedades

    Route::get('/propiedades/detalles/{id_propiedad}', [DetallesPropiedadesController::class, 'getDetalles'])->name('detalles_propiedades_home');

    Route::get('/propiedades/detalles/new/{id_propiedad}', [DetallesPropiedadesController::class, 'newDetalle']);

    Route::get('/propiedades/detalles/edit/{id}', [DetallesPropiedadesController::class, 'editDetalle']);

    Route::post('/propiedades/detalles', [DetallesPropiedadesController::class, 'customSaveDetalle']);

    Route::get('/propiedades/detalles/delete/{id}', [DetallesPropiedadesController::class, 'customDeleteDetalle']);

    // Imagenes de propiedades

    Route::get('/propiedades/imagenes/{id_propiedad}', [ImagenesPropiedadesController::class, 'getImagenes'])->name('imagenes_propiedades_home');

    Route::get('/propiedades/imagenes/new/{id_propiedad}', [ImagenesPropiedadesController::class, 'newImagen']);

    Route::get('/propiedades/imagenes/edit/{id}', [ImagenesPropiedadesController::class, 'editImagen']);

    Route::post('/propiedades/imagenes', [ImagenesPropiedadesController::class, 'customSaveImagen']);

    Route::get('/propiedades/imagenes/delete/{id}', [ImagenesPropiedadesController::class, 'customDeleteImagen']);

    // Categorias

    Route::get('/categorias',  [CategoriasController::class, 'getCategorias'])->name('categorias_home');

    Route::get('/categorias/new', [CategoriasController::class, 'newCategoria']);

    Route::get('/categorias/{id}', [CategoriasController::class, 'editCategoria']);

    Route::post('/categorias', [CategoriasController::class, 'customSaveCategoria']);

    Route::get('/categorias/delete/{id}', [CategoriasController::class, 'customDeleteCategoria']);

    // Noticias

    Route::get('/noticias', [NoticiasController::class, 'getNoticias'])->name('noticias_home');

    Route::get('/noticias/new', [NoticiasController::class, 'newNoticia']);

    Route::get('/noticias/{id}', [NoticiasController::class, 'editNoticia']);

    Route::post('/noticias', [NoticiasController::class, 'customSaveNoticia']);

    Route::get('/noticias/delete/{id}', [NoticiasController::class, 'customDeleteNoticia']);

    // Testimonials

    Route::get('/testimonials', [TestimonialsController::class, 'getTestimonials'])->name('testimonials_home');

    Route::get('/testimonials/new', [TestimonialsController::class, 'newTestimonial']);

    Route::get('/testimonials/{id}', [TestimonialsController::class, 'editTestimonial']);

    Route::post('/testimonials', [TestimonialsController::class, 'customSaveTestimonial']);

    Route::get('/testimonials/delete/{id}', [TestimonialsController::class, 'customDeleteTestimonial']);

    // Agentes

    Route::get('/agentes', [AgentesController::class, 'getAgentes'])->name('agentes_home');

    Route::get('/agentes/new', [AgentesController::class, 'newAgente']);

    Route::get('/agentes/{id}', [AgentesController::class, 'editAgente']);

    Route::post('/agentes', [AgentesController::class, 'customSaveAgente']);

    Route::get('/agentes/delete/{id}', [AgentesController::class, 'customDeleteAgente']);
});
// Forgery

Route::get('/token', function () {
    return csrf_token();
});

// Testing

Route::get('/send-mail', function () {

    $details = [
        'title' => 'Mail from Codingspoint.com',
        'body' => 'This is for testing email using smtp'
    ];

    \Mail::to('a.ti.99@hotmail.com')->send(new Mailer($details));

    dd("Email is Sent.");
});
