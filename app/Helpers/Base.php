<?php

use App\Models\Propiedad;
use App\Models\Noticia;
use App\Models\Agente;

if(! function_exists('getLastProperties')){
    function getLastProperties(){
        $propiedades = Propiedad::orderBy('updated_at', 'DESC')->paginate(5);
        return $propiedades;
    }
    function getUltimasNoticias(){
        $noticias = Noticia::orderBy('updated_at', 'DESC')->paginate(5);
        return $noticias;
    }
    function getBestAgents(){
        $agentes = Agente::orderBy('updated_at', 'DESC')->paginate(3);
        return $agentes;
    }    
    function getBestProps(){
        $propiedades = Propiedad::orderBy('precio', 'DESC')->paginate(3);
        return $propiedades;
    }
}