<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetallePropiedad extends Model
{
    use HasFactory;
    protected $table = "detalles_propiedades";
    protected $primaryKey = "id_detalle_propiedad";
}
