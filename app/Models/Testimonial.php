<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use HasFactory;
    protected $primaryKey = "id_testimonial";
    protected $table = "testimonials";    
    protected $appends = ['url_imagen'];

    public function getUrlImagenAttribute(){
        return "/images/testimonials/".$this->id_testimonial."/".$this->imagen;
    }

}
