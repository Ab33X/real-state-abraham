<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Propiedad;

class Agente extends Model
{
    use HasFactory;

    protected $table = "agentes";
    protected $primaryKey = "id_agente";

    protected $appends = ["url_imagen"];

    public function getUrlImagenAttribute(){
        return "/images/agentes/".$this->id_agente."/".$this->imagen;
    }
    
    public function propiedades(){
        return $this->hasMany(Propiedad::class, "id_agente", "id_agente");
    }
}
