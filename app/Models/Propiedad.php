<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Categoria;
use App\Models\ImagenPropiedad;
use App\Models\Agente;

class Propiedad extends Model
{
    use HasFactory;
    use Sluggable;
   
    protected $table = 'propiedades';
    protected $primaryKey = 'id_propiedad';

    protected $appends = ['precio_formato', 'last_image'];
    
    public function sluggable(): array{
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }

    public function categoria(){
        return $this->belongsTo(Categoria::class, "id_categoria", "id_categoria");
    }

    public function agente(){
        return $this->belongsTo(Agente::class, "id_agente", "id_agente");
    }

    public function imagenes_propiedades(){
        return $this->hasMany(ImagenPropiedad::class, "id_propiedad", "id_propiedad");
    }

    public function detalles_propiedades(){
        return $this->hasMany(DetallePropiedad::class, "id_propiedad", "id_propiedad");
    }

    public function getLastImageAttribute(){
        $imagen = ImagenPropiedad::where('id_propiedad', '=', $this->id_propiedad)->orderBy('updated_at', 'DESC')->first();
        if(isset($imagen)){
            return "/images/propiedades/".$imagen->id_propiedad."/".$imagen->nombre_archivo;
        } else {
            return "/image-not-found.png";
        }
    }

    public function getPrecioFormatoAttribute(){
        return $this->precio." MXN";
    }
}
