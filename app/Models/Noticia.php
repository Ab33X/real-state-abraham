<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    
    use HasFactory;
    protected $primaryKey = "id_noticia";
    protected $table = "noticias";
    protected $appends = ['url_imagen'];

    public function getUrlImagenAttribute(){
        return "/images/noticias/".$this->id_noticia."/".$this->imagen;
    }

}
