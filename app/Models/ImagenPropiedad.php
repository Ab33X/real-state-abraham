<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Propiedad;

class ImagenPropiedad extends Model
{
    use HasFactory;

    protected $table = 'imagenes_propiedades';
    protected $primaryKey = 'id_imagen_propiedad';
    protected $appends = ['url_imagen'];

    public function getUrlImagenAttribute(){
        return "/images/propiedades/".$this->id_propiedad."/".$this->nombre_archivo;
    }

    public function propiedad(){
        return $this->belongsTo(Propiedad::class, "id_propiedad", "id_propiedad");
    }
}
