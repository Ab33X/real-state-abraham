<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    use HasFactory;
    protected $primaryKey = "id_mensaje";
    protected $table = "mensajes";

    public function propiedad(){
        return $this->belongsTo(Propiedad::class, "id_propiedad", "id_propiedad");
    }

    public function agente(){
        return $this->belongsTo(Agente::class, "id_agente", "id_agente");
    }
}
