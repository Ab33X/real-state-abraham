<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Noticia;

class NoticiasController extends Controller
{
    public function getBlog(Request $request){
        $data = Noticia::paginate(10);
        return view('propiedades.blog', ["noticias" => $data]);
    }

    public function getNoticias(Request $request)
    {
        $data = Noticia::paginate(5);
        return view('admin/noticias/vw_noticias', ['noticias' => $data]);
    }

    public function newNoticia(Request $request)
    {
        return view('admin/noticias/form_noticias');
    }

    public function editNoticia(Request $request)
    {
        $noticia = Noticia::find($request->id);
        return view('admin/noticias/form_noticias', ['noticia' => $noticia]);
    }

    public function customSaveNoticia(Request $request)
    {
        if(isset($request->fileImage)){
            $request->validate([
                'fileImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $request->imagen = time().'.'.$request->fileImage->extension();
        }
        if ($request->id_noticia) {
            return $this->saveNoticia($request);
        } else {
            return $this->insertNoticia($request);
        }
    }

    public function saveNoticia(Request $request)
    {
        $noticia = Noticia::find($request->id_noticia);
        $noticia->id_noticia = $request->id_noticia;
        $noticia->titulo = $request->titulo;
        $noticia->subtitulo = $request->subtitulo;
        $noticia->fecha_publicacion = $request->fecha_publicacion;
        $noticia->imagen = $request->imagen;
        if ($noticia->save()) {
            $noticia->status = "success";
            $noticia->message = "Noticia guardada";
        } else {
            $noticia->status = "error";
            $noticia->message = "La noticia no fue guardada";
        }
        if(isset($request->fileImage)){
            $request->fileImage->move(public_path('images/noticias/'.$noticia->id_noticia), $request->imagen);
        }
        return redirect()->route('noticias_home');
    }

    public function insertNoticia(Request $request)
    {
        $noticia = new Noticia;
        $noticia->titulo = $request->titulo;
        $noticia->subtitulo = $request->subtitulo;
        $noticia->fecha_publicacion = $request->fecha_publicacion;
        $noticia->imagen = $request->imagen;
        if ($noticia->save()) {
            $noticia->status = "success";
            $noticia->message = "Noticia guardada";
        } else {
            $noticia->status = "error";
            $noticia->message = "La noticia no fue guardada";
        }
        if(isset($request->fileImage)){
            $request->fileImage->move(public_path('images/noticias/'.$noticia->id_noticia), $request->imagen);
        }
        return redirect()->route('noticias_home');
    }

    public function customDeleteNoticia(Request $request)
    {
        $noticia = Noticia::find($request->id);
        if ($noticia->forceDelete()) {
            $noticia->status = "success";
            $noticia->message = "Noticia eliminada";
        } else {
            $noticia->status = "error";
            $noticia->message = "La noticia no fue eliminada";
        }
        return redirect()->route('noticias_home');
    }
}
