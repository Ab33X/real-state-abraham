<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agente;

class AgentesController extends Controller
{

    public function getEnrollment(){
        $data = Agente::paginate(10);
        return view('propiedades.agentes', ["agentes" => $data]);
    }

    // Admin functions

    public function getAgentes(Request $request)
    {
        $data = Agente::all();
        return view('admin/agentes/vw_agentes', ['agentes' => $data]);
    }

    public function newAgente(Request $request)
    {
        return view('admin/agentes/form_agentes');
    }

    public function editAgente(Request $request)
    {
        $agente = Agente::find($request->id);
        return view('admin/agentes/form_agentes', ['agente' => $agente]);
    }

    public function customSaveAgente(Request $request)
    {
        if(isset($request->fileImage)){
            $request->validate([
                'fileImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $request->imagen = time().'.'.$request->fileImage->extension();
        }
        if ($request->id_agente) {
            return $this->saveAgente($request);
        } else {
            return $this->insertAgente($request);
        }
    }

    public function saveAgente(Request $request)
    {
        $agente = Agente::find($request->id_agente);
        $agente->id_agente = $request->id_agente;
        $agente->nombre = $request->nombre;
        $agente->email = $request->email;
        $agente->telefono = $request->telefono;
        $agente->experiencia = $request->experiencia;
        $agente->imagen = $request->imagen;
        $agente->celular = $request->celular;
        $agente->skype = $request->skype;
        $agente->faceboook = $request->faceboook;
        $agente->twitter = $request->twitter;
        $agente->instagram = $request->instagram;
        $agente->pinterest = $request->pinterest;
        if ($agente->save()) {
            $agente->status = "success";
            $agente->message = "Agente guardado";
        } else {
            $agente->status = "error";
            $agente->message = "El agente no fue guardado";
        }
        if(isset($request->fileImage)){
            $request->fileImage->move(public_path('images/agentes/'.$agente->id_agente), $request->imagen);
        }
        return redirect()->route('agentes_home');
    }

    public function insertAgente(Request $request)
    {
        $agente = new Agente;
        $agente->nombre = $request->nombre;
        $agente->email = $request->email;
        $agente->telefono = $request->telefono;
        $agente->experiencia = $request->experiencia;
        $agente->imagen = $request->imagen;
        $agente->celular = $request->celular;
        $agente->skype = $request->skype;
        $agente->faceboook = $request->faceboook;
        $agente->twitter = $request->twitter;
        $agente->instagram = $request->instagram;
        $agente->pinterest = $request->pinterest;
        if ($agente->save()) {
            $agente->status = "success";
            $agente->message = "Agente guardado";
        } else {
            $agente->status = "error";
            $agente->message = "El agente no fue guardado";
        }
        if(isset($request->fileImage)){
            $request->fileImage->move(public_path('images/agentes/'.$agente->id_agente), $request->imagen);
        }
        return redirect()->route('agentes_home');
    }

    public function customDeleteCategoria(Request $request)
    {
        $agente = Agente::find($request->id);
        if ($agente->forceDelete()) {
            $agente->status = "success";
            $agente->message = "Agente eliminado";
        } else {
            $agente->status = "error";
            $agente->message = "El agente no fue eliminado";
        }
        return redirect()->route('agentes_home');
    }
}
