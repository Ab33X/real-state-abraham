<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Propiedad;
use App\Models\Categoria;
use App\Models\Agente;

class PropiedadesController extends Controller
{
    public function detailPropiedad(Request $request, $slug)
    {
        $propiedad = Propiedad::where('slug', $slug)->first();
        if (!$propiedad) {
            return abort(404);
        }
        return view('propiedades.detail', ['propiedad'=>$propiedad]);
    }

    public function getProps(Request $request){
        $data = Propiedad::paginate(10);
        return view('propiedades.props', ["propiedades" => $data]);
    }

    // Admin functions

    public function getPropiedades(Request $request)
    {
        $data = Propiedad::paginate(5);
        return view('admin.propiedades.vw_propiedades', ["propiedades" => $data]);
    }

    public function newPropiedad(Request $request)
    {
        $agentes = Agente::all();
        $categorias = Categoria::all();
        return view('admin.propiedades.form_propiedades', ['categorias' => $categorias, 'agentes' => $agentes]);
    }

    public function editPropiedad(Request $request)
    {
        $propiedad = Propiedad::find($request->id);
        $agentes = Agente::all();
        $categorias = Categoria::all();
        return view('admin.propiedades.form_propiedades', ['categorias' => $categorias, 'agentes' => $agentes, 'propiedad' => $propiedad]);
    }

    public function customSavePropiedad(Request $request)
    {
        if ($request->id_propiedad) {
            return $this->savePropiedad($request);
        } else {
            return $this->insertPropiedad($request);
        }
    }

    public function insertPropiedad(Request $request)
    {
        $propiedad = new Propiedad;
        $propiedad->titulo = $request -> titulo;
        $propiedad->descripcion = $request -> descripcion;
        $propiedad->precio = $request -> precio;
        $propiedad->localizacion = $request -> localizacion;
        $propiedad->area = $request -> area;
        $propiedad->cuartos = $request -> cuartos;
        $propiedad->banios = $request -> banios;
        $propiedad->garages = $request -> garages;
        $propiedad->id_categoria = $request -> id_categoria;
        $propiedad->id_agente = $request -> id_agente;
        if ($propiedad->save()) {
            $propiedad->status = "success";
            $propiedad->message = "Propiedad guardada";
        } else {
            $propiedad->status = "error";
            $propiedad->message = "La propiedad no fue guardada";
        }
        return redirect()->route('propiedades_home');
    }

    public function savePropiedad(Request $request)
    {
        $propiedad = Propiedad::find($request->id_propiedad);
        $propiedad->titulo = $request -> titulo;
        $propiedad->descripcion = $request -> descripcion;
        $propiedad->precio = $request -> precio;
        $propiedad->localizacion = $request -> localizacion;
        $propiedad->area = $request -> area;
        $propiedad->cuartos = $request -> cuartos;
        $propiedad->banios = $request -> banios;
        $propiedad->garages = $request -> garages;
        $propiedad->id_categoria = $request -> id_categoria;
        $propiedad->id_agente = $request -> id_agente;
        if ($propiedad->save()) {
            $propiedad->status = "success";
            $propiedad->message = "Propiedad guardada";
        } else {
            $propiedad->status = "error";
            $propiedad->message = "La propiedad no fue guardada";
        }
        return redirect()->route('propiedades_home');
    }

    public function customDeletePropiedad(Request $request)
    {
        $propiedad = Propiedad::find($request->id);
        if ($propiedad->forceDelete()) {
            $propiedad->status = "success";
            $propiedad->message = "Propiedad eliminada";
        } else {
            $propiedad->status = "error";
            $propiedad->message = "La propiedad no fue eliminada";
        }
        return redirect()->back();
    }
}
