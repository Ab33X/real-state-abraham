<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DetallePropiedad;
use App\Models\Propiedad;

class DetallesPropiedadesController extends Controller
{
    public function getDetalles(Request $request){
        $propiedad = Propiedad::find($request->id_propiedad);
        $data = DetallePropiedad::where('id_propiedad', '=', $request->id_propiedad)->paginate(5);
        return view('admin.propiedades.detalles.vw_detalles', ["propiedad" => $propiedad,"detalles" => $data]);
    }

    public function newDetalle(Request $request)
    {
        $propiedad = Propiedad::find($request->id_propiedad);
        return view('admin.propiedades.detalles.form_detalles', ["propiedad" => $propiedad]);
    }

    public function editDetalle(Request $request)
    {
        $detalle = DetallePropiedad::find($request->id);
        $propiedad = Propiedad::find($detalle->id_propiedad);
        return view('admin.propiedades.detalles.form_detalles', ["propiedad" => $propiedad, 'detalle' => $detalle]);
    }

    public function customSaveDetalle(Request $request)
    {
        if ($request->id_detalle_propiedad) {
            return $this->saveDetalle($request);
        } else {
            return $this->insertDetalle($request);
        }
    }

    public function saveDetalle(Request $request)
    {
        $detalle = DetallePropiedad::find($request->id_detalle_propiedad);
        $detalle->id_detalle_propiedad = $request->id_detalle_propiedad;
        $detalle->id_propiedad = $request->id_propiedad;
        $detalle->descripcion = $request->descripcion;
        if ($detalle->save()) {
            $detalle->status = "success";
            $detalle->message = "Detalle de propiedad guardado";
        } else {
            $detalle->status = "error";
            $detalle->message = "El detalle de la propiedad no fue guardado";
        }
        return redirect()->route('detalles_propiedades_home', ['id_propiedad' => $detalle->id_propiedad]);
    }

    public function insertDetalle(Request $request)
    {
        $detalle = new DetallePropiedad;
        $detalle->id_propiedad = $request->id_propiedad;
        $detalle->descripcion = $request->descripcion;
        if ($detalle->save()) {
            $detalle->status = "success";
            $detalle->message = "Detalle de propiedad guardado";
        } else {
            $detalle->status = "error";
            $detalle->message = "El detalle de la propiedad no fue guardado";
        }
        return redirect()->route('detalles_propiedades_home', ['id_propiedad' => $detalle->id_propiedad]);
    }

    public function customDeleteDetalle(Request $request)
    {
        $detalle = DetallePropiedad::find($request->id);
        if ($detalle->forceDelete()) {
            $detalle->status = "success";
            $detalle->message = "Detalle de propiedad eliminado";
        } else {
            $detalle->status = "error";
            $detalle->message = "El detalle de la propiedad no fue eliminado";
        }
        return redirect()->back();
    }
}
