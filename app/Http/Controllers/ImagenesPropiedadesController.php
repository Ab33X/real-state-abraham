<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ImagenPropiedad;
use App\Models\Propiedad;

class ImagenesPropiedadesController extends Controller
{
    function getImagenes(Request $request)
    {
        $propiedad = Propiedad::find($request->id_propiedad);
        $data = ImagenPropiedad::where('id_propiedad', '=', $request->id_propiedad)->paginate(6);
        return view('admin.propiedades.imagenes.vw_imagenes', ["propiedad" => $propiedad, "imagenes" => $data]);
    }

    public function newImagen(Request $request)
    {
        $propiedad = Propiedad::find($request->id_propiedad);
        return view('admin.propiedades.imagenes.form_imagenes', ["propiedad" => $propiedad]);
    }

    public function editImagen(Request $request)
    {
        $imagen = ImagenPropiedad::find($request->id);
        $propiedad = Propiedad::find($imagen->id_propiedad);
        return view('admin.propiedades.imagenes.form_imagenes', ["propiedad" => $propiedad, 'imagen' => $imagen]);
    }

    public function customSaveImagen(Request $request)
    {
        if(isset($request->fileImage)){
            $request->validate([
                'fileImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $request->nombre_archivo = time().'.'.$request->fileImage->extension();
            $request->extension = $request->fileImage->extension();
        }
        if ($request->id_imagen_propiedad) {
            return $this->saveImagen($request);
        } else {
            return $this->insertImagen($request);
        }
    }

    function insertImagen(Request $request){
        $imagen = new ImagenPropiedad;
        $imagen->titulo_imagen = $request->titulo_imagen;
        $imagen->id_propiedad = $request->id_propiedad;
        $imagen->nombre_archivo = $request->nombre_archivo;
        $imagen->extension = $request->extension;
        if ($imagen->save()) {
            $imagen->status = "success";
            $imagen->message = "Imagen de propiedad guardada";
        } else {
            $imagen->status = "error";
            $imagen->message = "Imagen de propiedad no fue guardada";
        }
        if(isset($request->fileImage)){
            $request->fileImage->move(public_path('images/propiedades/'.$imagen->id_propiedad), $request->nombre_archivo);
        }
        return redirect()->route('imagenes_propiedades_home', ['id_propiedad' => $imagen->id_propiedad]);;
    }

    function saveImagen(Request $request){
        $imagen = ImagenPropiedad::find($request->id_imagen_propiedad);
        $imagen->imagen = $request->id_imagen_propiedad;
        $imagen->titulo_imagen = $request->titulo_imagen;
        $imagen->id_propiedad = $request->id_propiedad;
        $imagen->nombre_archivo = $request->nombre_archivo;
        $imagen->extension = $request->extension;
        if ($imagen->save()) {
            $imagen->status = "success";
            $imagen->message = "Imagen de propiedad guardada";
        } else {
            $imagen->status = "error";
            $imagen->message = "Imagen de propiedad no fue guardada";
        }
        if(isset($request->fileImage)){
            $request->fileImage->move(public_path('images/propiedades/'.$imagen->id_propiedad), $request->nombre_archivo);
        }
        return redirect()->route('imagenes_propiedades_home', ['id_propiedad' => $imagen->id_propiedad]);;
    }

    public function customDeleteImagen(Request $request)
    {
        $imagen = ImagenPropiedad::find($request->id);
        if ($imagen->forceDelete()) {
            $imagen->status = "success";
            $imagen->message = "Imagen de propiedad eliminada";
        } else {
            $imagen->status = "error";
            $imagen->message = "La imagen de la propiedad no fue eliminada";
        }
        return redirect()->back();
    }
}
