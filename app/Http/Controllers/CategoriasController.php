<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categoria;

class CategoriasController extends Controller
{
    public function getCategorias(Request $request)
    {
        $data = Categoria::all();
        return view('admin/categorias/vw_categorias', ['categorias' => $data]);
    }

    public function newCategoria(Request $request)
    {
        return view('admin/categorias/form_categorias');
    }

    public function editCategoria(Request $request)
    {
        $categoria = Categoria::find($request->id);
        return view('admin/categorias/form_categorias', ['categoria' => $categoria]);
    }

    public function customSaveCategoria(Request $request)
    {
        if ($request->id_categoria) {
            return $this->saveCategoria($request);
        } else {
            return $this->insertCategoria($request);
        }
    }

    public function saveCategoria(Request $request)
    {
        $categoria = Categoria::find($request->id_categoria);
        $categoria->id_categoria = $request->id_categoria;
        $categoria->categoria = $request->categoria;
        if ($categoria->save()) {
            $categoria->status = "success";
            $categoria->message = "Categoria guardada";
        } else {
            $categoria->status = "error";
            $categoria->message = "La categoria no fue guardada";
        }
        return redirect()->route('categorias_home');
    }

    public function insertCategoria(Request $request)
    {
        $categoria = new Categoria;
        $categoria->categoria = $request->categoria;
        if ($categoria->save()) {
            $categoria->status = "success";
            $categoria->message = "Categoria guardada";
        } else {
            $categoria->status = "error";
            $categoria->message = "La categoria no fue guardada";
        }
        return redirect()->route('categorias_home');
    }

    public function customDeleteCategoria(Request $request)
    {
        $categoria = Categoria::find($request->id);
        if ($categoria->forceDelete()) {
            $categoria->status = "success";
            $categoria->message = "Categoria eliminada";
        } else {
            $categoria->status = "error";
            $categoria->message = "La categoria no fue eliminada";
        }
        return redirect()->route('categorias_home');
    }
}
