<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;

class TestimonialsController extends Controller
{
    public function getAbout(Request $request){
        $data = Testimonial::orderBy('updated_at','DESC')->paginate(3);
        return view('propiedades.about', ["testimonials"=>$data]);
    }

    public function getTestimonials(Request $request){
        $data = Testimonial::paginate(10);
        return view('admin.testimonials.vw_testimonials', ["testimonials" => $data]);
    }

    public function newTestimonial(Request $request)
    {
        return view('admin.testimonials.form_testimonials');
    }

    public function editTestimonial(Request $request)
    {
        $testimonial = Testimonial::find($request->id);
        return view('admin.testimonials.form_testimonials', ['testimonial' => $testimonial]);
    }

    public function customSaveTestimonial(Request $request)
    {
        if(isset($request->fileImage)){
            $request->validate([
                'fileImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $request->imagen = time().'.'.$request->fileImage->extension();
        }
        if ($request->id_testimonial) {
            return $this->saveTestimonial($request);
        } else {
            return $this->insertTestimonial($request);
        }
    }

    public function saveTestimonial(Request $request)
    {
        $testimonial = Testimonial::find($request->id_testimonial);
        $testimonial->id_testimonial = $request->id_testimonial;
        $testimonial->title = $request->title;
        $testimonial->comment = $request->comment;
        $testimonial->imagen = $request->imagen;
        if ($testimonial->save()) {
            $testimonial->status = "success";
            $testimonial->message = "Testimonial guardado";
        } else {
            $testimonial->status = "error";
            $testimonial->message = "El testimonial no fue guardado";
        }
        if(isset($request->fileImage)){
            $request->fileImage->move(public_path('images/testimonials/'.$testimonial->id_testimonial), $request->imagen);
        }
        return redirect()->route('testimonials_home');
    }

    public function insertTestimonial(Request $request)
    {
        $testimonial = new Testimonial;
        $testimonial->title = $request->title;
        $testimonial->comment = $request->comment;
        $testimonial->imagen = $request->imagen;
        if ($testimonial->save()) {
            $testimonial->status = "success";
            $testimonial->message = "Testimonial guardado";
        } else {
            $testimonial->status = "error";
            $testimonial->message = "El testimonial no fue guardado";
        }
        if(isset($request->fileImage)){
            $request->fileImage->move(public_path('images/testimonials/'.$testimonial->id_testimonial), $request->imagen);
        }
        return redirect()->route('testimonials_home');
    }

    public function customDeleteTestimonial(Request $request)
    {
        $testimonial = Testimonial::find($request->id);
        if ($testimonial->forceDelete()) {
            $testimonial->status = "success";
            $testimonial->message = "Testimonial eliminado";
        } else {
            $testimonial->status = "error";
            $testimonial->message = "El testimonial no fue eliminado";
        }
        return redirect()->route('testimonials_home');
    }
}
