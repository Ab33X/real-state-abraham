<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mensaje;

class MensajesController extends Controller
{
    public function saveMensaje(Request $request){
        $mensaje = new Mensaje;
        $mensaje->nombre = $request->nombre;
        $mensaje->correo = $request->correo;
        $mensaje->comentario = $request->comentario;
        $mensaje->id_propiedad = $request->id_propiedad;
        $mensaje->id_agente = $request->id_agente;
        if($mensaje->save()){
            $mensaje->message = "Success";
        } else {
            $mensaje->message = "Error";
        }
        return redirect()->back();
    }

    public function getMensajes(Request $request){
        $data = Mensaje::orderBy('created_at')->get();
        return view('admin/mensajes/vw_mensajes', ["mensajes" => $data]);
    }
}
